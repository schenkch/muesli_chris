/****************************************************************************
*
*                                 M U E S L I   v 1.8
*
*
*     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/


#include <iostream>
#include <iomanip>
#include <cstdlib>
#include "muesli/muesli.h"

/* this function runs a cyclic test on an elastoplastic material
   point, and outputs results to a data for later plotting
*/
int main(int argc, char **argv)
{
  /****  Neo Hookean Material  *****/
  const double E = 200e9; // Young's modulus
  const double nu = 0.3;   // Poisson's ratio
  const double rho = 1.0;   // density
  const std::string namenh = "a classic Neo-Hooke mat";

  muesli::neohookeanMaterial matNH(namenh, E, nu, rho);
  muesli::finiteStrainMP* pNH = matNH.createMaterialPoint();

  /****  Neural Network material 1  ****/
  const std::string filename = "nnarchitecture.dat";
/*
  muesli::nnet NeuralNet(filename);
  // Generation of file with neural network info
  std::ofstream outfile("NNinfo.txt");
  NeuralNet.info(outfile);
  std::cout << layerSize[l] << std::endl;
*/    
  const std::string namenn1 = "a NeuralNetwork material";  
  muesli::nnFsolid matNN1(namenn1, filename);
  muesli::finiteStrainMP* pNN1 = matNN1.createMaterialPoint();

  // Generation of file with info of the material point and the NN
  std::ofstream outfileMP("NNMPinfo.txt");
  matNN1.print(outfileMP);
      
   
//int l=0,i=0,j=0;
//double w = NeuralNet.weights[l](i,j), b = NeuralNet.biases[l][j]; 
//std::cout << "layer = " << l  << " , bias[" << j << "] = " << b << " , weights ["  << i << "," << j << "] = " << w << std::endl;
//std::cout << NeuralNet.activation[l] << std::endl;

  itensor F; F = F.identity();
  double a, amax = 0.02;
  unsigned npercycle = 40;
  unsigned ncycles   = 1;


  //std::ofstream os("input.data");

/****** Problema 1: ******/
  /* F = [1+a 0  0       C = [(1+a)^2 0 0
           0  1  0  -->          0    1 0
           0  0  1]              0    0 1]*/

  /**** Output file  ****/
  std::ofstream os1("test1.data");

  // double acumSqDiff = 0, acumSqEner = 0; 

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,0) = (1 - amax + a);
          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  << 
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */

          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy(); 
          istensor S_NH, S_NH_num; 
          pNH->secondPiolaKirchhoffStress(S_NH);
          // pNH->secondPiolaKirchhoffStressNumerical(S_NH_num);
          pNH->commitCurrentState();
          // std::cout << "Neo-Hookean 2nd P-K: S = " << S_NH << std::endl;
          // std::cout << "Neo-Hookean numerical 2nd P-K: S = " << S_NH_num << std::endl;
          // std::cout << "Neo-Hookean energy: w = " << energyNH << std::endl;

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy(); 
          // istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          //itensor P; pNN1->firstPiolaKirchhoffStress(P);
          //double   S11 = S(0,0);
          //double   S22 = S(1,1);
          //double   W   = pNN1->storedEnergy();
          pNN1->commitCurrentState();
          // std::cout << "Neural Network 2nd P-K: S = " << S_NN << std::endl;
          // std::cout << "Neural Network energy: w = " << energyNN1 << std::endl;

          // acumSqDiff = acumSqDiff + (energyNH-energyNN1)*(energyNH-energyNN1);
          // acumSqEner = acumSqEner + energyNH*energyNH;
          // std::cout << "Acumm error = " << acumSqDiff/acumSqEner << std::endl;

          os1 << "\n" << std::setw(8) << std::scientific << - amax + a << " "  << energyNH << " "  << energyNN1;

      }

    os1.close();

/****** Problema 2: ******/
  /* F = [1+a 0  0       C = [(1+a)^2    0    0
           0 1+a 0  -->          0    (1+a)^2 0
           0  0  1]              0       0    1]*/

  /**** Output file  ****/
  std::ofstream os2("test2.data");

  // double acumSqDiff = 0, acumSqEner = 0; 
  
  F = F.identity();

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,0) = F(1,1) = (1 - amax + a);

          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  << 
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */

          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy(); 
          istensor S_NH, S_NH_num; 
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy(); 
          //istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          pNN1->commitCurrentState();

          os2 << "\n" << std::setw(8) << std::scientific << -amax + a << " "  << energyNH << " "  << energyNN1;
      }

    os2.close();  

/****** Problema 3: ******/
  /* F = [1 a 0       C = [1   a   0
          0 1 0  -->       a 1+a^2 0
          0 0 1]           0   0   1]*/

  /**** Output file  ****/
  std::ofstream os3("test3.data");

  // double acumSqDiff = 0, acumSqEner = 0; 
  F = F.identity();

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,1) = - amax + a;

          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  << 
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */


          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy(); 
          istensor S_NH, S_NH_num; 
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy(); 
          //istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          pNN1->commitCurrentState();

          os3 << "\n" << std::setw(8) << std::scientific << - amax + a << " "  << energyNH << " "  << energyNN1;
      }

    os3.close();  

/****** Problema 4: ******/
  /* F = [ 1  -a   0       C = [1+a^2    -a+a^2      a-a^2
           0  1+a 2*a  -->     -a+a^2  1+2a+3a^2    3a+a^2
           a   a  1-a]          a-a^2    3a+a^2   1-2a+5a^2]*/

  /**** Output file  ****/
  std::ofstream os4("test4.data");

  // double acumSqDiff = 0, acumSqEner = 0; 
  F = F.identity();

    for (unsigned i=0; i<=ncycles*npercycle; i++)
      {
          // double t = sin(((double)i)/npercycle*2.0*M_PI);
          a = (double)i/npercycle*amax*2;
          F(0,1) = -(-amax+a); F(1,1) = (1+(-amax+a)); F(1,2) = 2*(-amax+a);
          F(2,0) = F(2,1) = (-amax+a); F(2,2) = 1-(-amax+a);

          /*
          istensor Cc = istensor::tensorTransposedTimesTensor(F);
          std::cout << Cc << std::endl;
          os  << "\n" << std::setw(8) << std::scientific << Cc(0,0) << " "  << Cc(1,1) << " "  << 
            Cc(2,2) << " "  << Cc(1,2) << " "  << Cc(0,2) << " "  << Cc(0,1);
            */

          /* Calculations in Neo Hookean material*/
          pNH->updateCurrentState(i, F);
          double energyNH = pNH->storedEnergy(); 
          istensor S_NH, S_NH_num; 
          pNH->secondPiolaKirchhoffStress(S_NH);
          pNH->commitCurrentState();

          /* Calculations in Neural Network material*/
          pNN1->updateCurrentState(i, F);
          double energyNN1 = pNN1->storedEnergy(); 
          //istensor S_NN; pNN1->secondPiolaKirchhoffStress(S_NN);
          pNN1->commitCurrentState();

          os4 << "\n" << std::setw(8) << std::scientific << (-amax+a) << " "  << energyNH << " "  << energyNN1;
      }

    os4.close();  
    

delete pNH;
delete pNN1;

//os.close(); 

return 0;
}
