/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/



#pragma once
#ifndef __muesli_sphasemech_h
#define __muesli_sphasemech_h

#include "muesli/material.h"


namespace muesli
{
    class smallStrainMaterial;
    class smallStrainMP;
    class sPhaseMechMP;

    class sPhaseMechMaterial : public muesli::material
    {
    public:
                                sPhaseMechMaterial(const std::string& name,
                                                   const materialProperties& cl);
        virtual                 ~sPhaseMechMaterial();

        virtual bool            check() const;
        virtual sPhaseMechMP*   createMaterialPoint() const;
        virtual double          density() const;
        virtual double          getProperty(const propertyName p) const;
        virtual void            print(std::ostream &of=std::cout) const;
        double                  referenceTemperature() const;
        virtual void            setRandom();
        virtual bool            test(std::ostream& of=std::cout);
        virtual double          waveVelocity() const;

    private:
        smallStrainMaterial     *theSSMaterial;
        double                  tref, ell, gc, k0;

        friend class            sPhaseMechMP;
    };

    inline double sPhaseMechMaterial::referenceTemperature() const {return tref;}



    class sPhaseMechMP : public muesli::materialPoint
    {
    public:
                                sPhaseMechMP(const sPhaseMechMaterial &m);
        virtual                 ~sPhaseMechMP();

        double                  density() const;
        const sPhaseMechMaterial& parentMaterial() const;
        virtual void            setRandom();
        bool                    testImplementation(std::ostream& of=std::cout) const;

        // energies
        virtual double          freeEnergy() const;
        virtual double          deviatoricEnergy() const;
        virtual double          energyDissipationInStep() const;
        virtual double          effectiveStoredEnergy() const;
        double                  recoverableFreeEnergy() const;
        virtual double          volumetricEnergy() const;

        // gradients
        virtual double          pressure() const;
        virtual void            stress(istensor& sigma) const;
        virtual void            deviatoricStress(istensor& s) const;
        virtual double          plasticSlip() const;

        // tangents
        virtual void            contractWithTangent(const ivector &v1,
                                                    const ivector &v2,
                                                    itensor &T) const;
        virtual void            contractWithDeviatoricTangent(const ivector &v1,
                                                              const ivector &v2,
                                                              itensor &T) const;
        virtual void            dissipationTangent(itensor4& D) const;
        virtual void            tangentElasticities(itensor4& c) const;
        virtual double          volumetricStiffness() const;

        // bookkeeping
        virtual void            commitCurrentState();
        virtual istensor        getConvergedPlasticStrain() const;
        virtual materialState   getConvergedState() const;
        virtual istensor        getCurrentPlasticStrain() const;
        virtual materialState   getCurrentState() const;
        istensor&               getCurrentStrain();
        const istensor&         getCurrentStrain() const;
        double&                 phaseField();
        const double&           phaseField() const;
        virtual void            resetCurrentState();
        virtual void            updateCurrentState(const double t, const istensor& strain, const double phase, const ivector& gradPh);


    private:
        double                  phase_n, phase_c;
        ivector                 gradPh_n, gradPh_c;
        smallStrainMP*          theSSMP;
        const sPhaseMechMaterial&  thePhaseMechMaterial;
    };

    inline double sPhaseMechMP::density() const  {return thePhaseMechMaterial.density();}
    inline const sPhaseMechMaterial& sPhaseMechMP::parentMaterial() const {return thePhaseMechMaterial;}
}

#endif
