/****************************************************************************
*
*                                 M U E S L I   v 1.9
*
*
*     Copyright 2023 IMDEA Materials Institute, Getafe, Madrid, Spain
*     Contact: muesli.materials@imdea.org
*     Author: Ignacio Romero (ignacio.romero@imdea.org)
*
*     This file is part of MUESLI.
*
*     MUESLI is free software: you can redistribute it and/or modify
*     it under the terms of the GNU General Public License as published by
*     the Free Software Foundation, either version 3 of the License, or
*     (at your option) any later version.
*
*     MUESLI is distributed in the hope that it will be useful,
*     but WITHOUT ANY WARRANTY; without even the implied warranty of
*     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*     GNU General Public License for more details.
*
*     You should have received a copy of the GNU General Public License
*     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
*
****************************************************************************/

#include "species.h"

using namespace muesli;



speciesMaterial::speciesMaterial(const std::string& name):
    material(name)
 {
 }




speciesMaterial::speciesMaterial(const std::string& name,
                                 const materialProperties& cl):
material(name, cl)
{
}




speciesMaterial::~speciesMaterial()
{
}




void speciesMaterial::print(std::ostream &of) const
{
    material::print(of);
}




speciesMP::speciesMP(const speciesMaterial& mat):
    theSpeciesMaterial(&mat),
    time_n(0.0), mu_n(0.0), chi_n(0.0), gradMu_n(0.0, 0.0, 0.0),
    time_c(0.0), mu_c(0.0), chi_c(0.0), gradMu_c(0.0, 0.0, 0.0)
{
}




void speciesMP::commitCurrentState()
{
    time_n   = time_c;
    mu_n     = mu_c;
    chi_n    = chi_c;
    gradMu_n = gradMu_c;
}





materialState speciesMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theDouble.push_back(mu_n);
    state.theDouble.push_back(chi_n);
    state.theVector.push_back(gradMu_n);

    return state;
}




materialState speciesMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theDouble.push_back(mu_c);
    state.theDouble.push_back(chi_c);
    state.theVector.push_back(gradMu_c);

    return state;
}




void speciesMP::resetCurrentState()
{
    time_c   = time_n;
    mu_c     = mu_n;
    chi_c    = chi_n;
    gradMu_c = gradMu_n;
}




void speciesMP::setRandom()
{
    mu_n  = 0.0;
    mu_c  = 0.0;
    chi_n = 0.0;
    chi_c = 0.0;
    gradMu_n.setRandom();
    gradMu_c.setRandom();
}




bool speciesMP::testImplementation(std::ostream& os) const
{
    bool isok = true;
    speciesMP& theMP = const_cast<speciesMP&>(*this);

    // set a random update in the material
    double tn1 = muesli::randomUniform(0.1,1.0);

    ivector gradMu;
    gradMu.setRandom();

    double mu;
    mu = randomUniform(-20000.0, 20000.0);
    
    theMP.updateCurrentState(tn1, mu, gradMu);
    
    // Derivatives with respect to mu
    if (true)
    {
        // Compute numerical value of concentration chi =  -(d GCP / d mu)
        const double inc = 1.0e-4;
        
        double num_chi;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double GCP_p1          = grandCanonicalPotential();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double GCP_p2          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double GCP_m1          = grandCanonicalPotential();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double GCP_m2          = grandCanonicalPotential();
        
        // fourth order approximation of the derivative
        num_chi = - (-GCP_p2 + 8.0*GCP_p1 - 8.0*GCP_m1 + GCP_m2)/(12.0*inc);

        // programmed chemicalCapacity
        double pr_chi = concentration();

        // compare concentration with derivative of GCP wrt mu
        // relative error less than 0.01%
        double error = num_chi - pr_chi;
        
        if (fabs(pr_chi) < 1e-3)
        {
            isok = (fabs(error) < 1e-4);
            os << "\n   1. Comparing concentration with - [d GCP / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";

            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in concentration computation %e " << fabs(error)/fabs(pr_chi);
                os << "\n Programmed concentration: " << pr_chi;
                os << "\n Numeric concentration:    " << num_chi;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_chi) < 1e-4);
            os << "\n   1. Comparing concentration with - [d GCP / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";

            }
            
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in concentration computation %e " << fabs(error)/fabs(pr_chi);
                os << "\n Programmed concentration: " << pr_chi;
                os << "\n Numeric concentration:    " << num_chi;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of chemical capacity  chemCap = d chi / d mu
        const double inc = 1.0e-3;
        
        double num_chemCap;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double chi_p1          = concentration();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double chi_p2          = concentration();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double chi_m1          = concentration();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double chi_m2          = concentration();
        
        // fourth order approximation of the derivative
        num_chemCap = (-chi_p2 + 8.0*chi_p1 - 8.0*chi_m1 + chi_m2)/(12.0*inc);

        // programmed chemicalCapacity
        double pr_chemCap = chemicalCapacity();
    
        // compare chemical capacity with derivative of concentration wrt mu
        // relative error less than 0.01%
        double error = num_chemCap - pr_chemCap;
        
        if (fabs(num_chemCap) == 0.0)
        {
            isok = (fabs(error)< 1e-11);
            os << "\n   2. Comparing chemicalCapacity with  [d chi / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
                os << "\n Programmed chemical capacity: " << pr_chemCap;
                os << "\n Numeric chemical capacity:    " << num_chemCap;
            }
        }
        
        else if (fabs(pr_chemCap) < 5e-8)
        {
            isok = (fabs(num_chemCap)< 1e-7);
            os << "\n   2. Comparing chemicalCapacity with  [d chi / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
                os << "\n Programmed chemical capacity: " << pr_chemCap;
                os << "\n Numeric chemical capacity:    " << num_chemCap;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_chemCap) < 5e-4);
            os << "\n   2. Comparing chemicalCapacity with  [d chi / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacity computation %e " << fabs(error)/fabs(pr_chemCap);
                os << "\n Programmed chemical capacity: " << pr_chemCap;
                os << "\n Numeric chemical capacity:    " << num_chemCap;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of chemical capacity derivative chemCapDer = d chemCap / d mu
        const double inc = 1.0e-3;
        
        double num_chemCapDer;
                
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double chemCap_p1    = chemicalCapacity();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double chemCap_p2    = chemicalCapacity();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double chemCap_m1    = chemicalCapacity();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double chemCap_m2    = chemicalCapacity();
        
        // fourth order approximation of the derivative
        num_chemCapDer = (-chemCap_p2 + 8.0*chemCap_p1 - 8.0*chemCap_m1 + chemCap_m2)/(12.0*inc);

        // programmed chemicalCapacityDerivative
        double pr_chemCapDer = chemicalCapacityDerivative();

        // Compare chemical capacity derivative with derivative of chemical capacity wrt mu
        // relative error less than 0.01%
        double error = num_chemCapDer - pr_chemCapDer;
        
        if (fabs(pr_chemCapDer) == 0.0)
        {
            isok = (fabs(error)< 1e-13);
            os << "\n   3. Comparing chemicalCapacityDerivative with [ d chemCap / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacityDerivative computation %e " << fabs(error)/fabs(pr_chemCapDer);
                os << "\n Programmed chemical capacity derivative: " << pr_chemCapDer;
                os << "\n Numeric chemical capacity derivative:    " << num_chemCapDer;
            }
        }
        
        else
        {
            isok = (fabs(error)/fabs(pr_chemCapDer) < 1e-4);
            os << "\n   3. Comparing chemicalCapacityDerivative with [ d chemCap / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in chemicalCapacityDerivative computation %e " << fabs(error)/fabs(pr_chemCapDer);
                os << "\n Programmed chemical capacity derivative: " << pr_chemCapDer;
                os << "\n Numeric chemical capacity derivative:    " << num_chemCapDer;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of mobility derivative mobilityDerivative = -(d mobility / d mu)
        const double inc = 1.0e-3;
        
        double num_mDer;
        
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        double mDer_p1    = mobility();
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        double mDer_p2    = mobility();

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        double mDer_m1    = mobility();

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        double mDer_m2    = mobility();
        
        // fourth order approximation of the derivative
        num_mDer = (-mDer_p2 + 8.0*mDer_p1 - 8.0*mDer_m1 + mDer_m2)/(12.0*inc);

        // programmed mobilityDerivative
        double pr_mDer = mobilityDerivative();
        
        // compare mobilityDerivative with derivative of mobility wrt mu
        // relative error less than 0.01%
        double error = num_mDer - pr_mDer;
        
        if (fabs(pr_mDer) == 0.0)
        {
            isok = (fabs(error)< 2e-13);
            os << "\n   4. Comparing mobilityDerivative with [d m / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in mobilityDerivative computation %e " << fabs(error)/fabs(pr_mDer);
                os << "\n Programmed mobility derivative: " << pr_mDer;
                os << "\n Numeric mobility derivative:    " << num_mDer;
            }
        }
        
        else if (pr_mDer < 1e-9)
        {
            isok = (fabs(error) < 1e-10);
            os << "\n   4. Comparing mobilityDerivative with [d m / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in mobilityDerivative computation %e " << fabs(error)/fabs(pr_mDer);
                os << "\n Programmed mobility derivative: " << pr_mDer;
                os << "\n Numeric mobility derivative:    " << num_mDer;
            }
        }
            
        else
        {
            isok = (fabs(error)/fabs(pr_mDer) < 1e-4);
            os << "\n   4. Comparing mobilityDerivative with [d m / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in mobilityDerivative computation %e " << fabs(error)/fabs(pr_mDer);
                os << "\n Programmed mobility derivative: " << pr_mDer;
                os << "\n Numeric mobility derivative:    " << num_mDer;
            }
        }
    }
    
    if (true)
    {
        // Compute numerical value of mass flux derivative wrt mu massFluxDerivative = d massFlux / d mu
        const double inc = 1.0e-3;
        
        ivector num_jDer;
        
        theMP.updateCurrentState(tn1, mu+inc, gradMu);
        ivector jDer_p1; massFlux(jDer_p1);
        
        theMP.updateCurrentState(tn1, mu+2.0*inc, gradMu);
        ivector jDer_p2; massFlux(jDer_p2);

        theMP.updateCurrentState(tn1, mu-inc, gradMu);
        ivector jDer_m1; massFlux(jDer_m1);

        theMP.updateCurrentState(tn1, mu-2.0*inc, gradMu);
        ivector jDer_m2; massFlux(jDer_m2);
        
        // fourth order approximation of the derivative
        num_jDer = (-jDer_p2 + 8.0*jDer_p1 - 8.0*jDer_m1 + jDer_m2)/(12.0*inc);

        // programmed massFluxDerivative
        ivector pr_jDer;
        massFluxDerivative(pr_jDer);
        
        // compare mobilityDerivative with derivative of mobility wrt mu
        // relative error less than 0.01%
        ivector error = num_jDer - pr_jDer;
    
        if (pr_jDer.norm() == 0.0)
        {
            isok = (error.norm() < 5e-13);
            os << "\n   5. Comparing massFluxDerivative with [d massFlux / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in massFluxDerivative computation %e " << error.norm()/pr_jDer.norm();
                os << "\n Programmed mass flux derivative: " << pr_jDer;
                os << "\n Numeric mass flux derivative:    " << num_jDer;
            }
        }
        
        else if (pr_jDer.norm() < 1e-9)
        {
            isok = (error.norm() < 1e-10);
            os << "\n   5. Comparing massFluxDerivative with [d massFlux / d mu ].";

            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in massFluxDerivative computation %e " << error.norm()/pr_jDer.norm();
                os << "\n Programmed mass flux derivative: " << pr_jDer;
                os << "\n Numeric mass flux derivative:    " << num_jDer;
            }
        }
        
        else
        {
            isok = (error.norm()/pr_jDer.norm() < 1e-4);
            os << "\n   5. Comparing massFluxDerivative with [d massFlux / d mu ].";
            
            if (isok)
            {
                os << " Test passed.";
            }
            else
            {
                os << "\n Test failed.";
                os << "\n Relative error in massFluxDerivative computation %e " << error.norm()/pr_jDer.norm();
                os << "\n Programmed mass flux derivative: " << pr_jDer;
                os << "\n Numeric mass flux derivative:    " << num_jDer;
            }
        }
    }
        
    // Derivatives with respect to gradMu
    if (true)
    {
        // Compute numerical value of mass flux massFlux = -(d kineticPotential / d gradMu)
        ivector num_massFlux;
        num_massFlux.setZero();
        const double inc = 1.0e-4;

        for (size_t i=0; i<3; i++)
        {
            double original = gradMu(i);
            
            gradMu(i) = original + inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_p1 = kineticPotential();

            gradMu(i) = original + 2.0*inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_p2 = kineticPotential();

            gradMu(i) = original - inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_m1 = kineticPotential();

            gradMu(i) = original - 2.0*inc;
            theMP.updateCurrentState(tn1, mu, gradMu);
            double Omega_m2 = kineticPotential();


            // fourth order approximation of the derivative
            double der = - (-Omega_p2 + 8.0*Omega_p1 - 8.0*Omega_m1 + Omega_m2)/(12.0*inc);
            num_massFlux(i) = der;

            gradMu(i) = original;
        }
        
        // programmed mass flux
        ivector pr_massFlux; massFlux(pr_massFlux);

        // compare mass flux with derivative of kinetic potential wrt gradMu

        // relative error less than 0.01%
        ivector error = num_massFlux - pr_massFlux;
        isok = (error.norm()/pr_massFlux.norm() < 1e-3);
        os << "\n   6. Comparing massFlux with [d Omega / d gradMu ].";

        if (isok)
        {
            os << " Test passed.";
        }
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in massFlux computation %e." <<  error.norm()/pr_massFlux.norm();
            os << "\n Programmed mass flux: " << pr_massFlux;
            os << "\n Numeric mass flux:    " << num_massFlux;
        }
    }
    
    if (true)
    {
        // Compute numerical value (approximation of derivative) of mobility  m = -(d j / d gradMu)
        const double inc = 1.0e-3;

        const size_t nnumder = 4.0;
        const double ndtimes[] = {+1.0, +2.0, -1.0, -2.0};
        const double ndfact[]  = {+8.0, -1.0, -8.0, +1.0};
        const double ndden = 12.0;
        istensor num_m;
        for (unsigned i=0; i<3; i++)
        {
            for (unsigned j=0; j<3; j++)
            {
                double der = 0.0;
                for (unsigned q=0; q<nnumder; q++)
                {
                    ivector gmu = gradMu_n;
                    gmu[j] = gradMu_n[j] + inc*ndtimes[q];
                    theMP.updateCurrentState(tn1, mu_n, gmu);
                    
                    ivector massFlux_aux; massFlux(massFlux_aux);
                    der += ndfact[q]*massFlux_aux(i);
                    theMP.resetCurrentState();
                }
                der /= inc*ndden;
                num_m(i,j) = -der;
            }
        }

        // compare programmed mobility with numerical derivative of mass flux wrt gradMu
        istensor pr_m = mobility()*istensor::identity();

        double error = (num_m - pr_m).norm();
        isok = (fabs(error)/pr_m.norm() < 1e-4);
            
        os << "\n   7. Comparing mobility with [d massFlux / d gradMu ].";
        if (isok)
        {
            os << " Test passed.";
        }
        
        else
        {
            os << "\n Test failed.";
            os << "\n Relative error in mobility computation %e." <<  fabs(error)/pr_m.norm();
            os << "\n Programmed mobility: " << pr_m;
            os << "\n Numeric mobility:    " << num_m.norm();
        }
    }
    
    return true;
}




void speciesMP::updateCurrentState(double theTime,
                                   double chemicalPot,
                                   const ivector& gradMu)
{
    time_c   = theTime;
    mu_c     = chemicalPot;
    gradMu_c = gradMu;
    chi_c    = concentration();
}




linearDiffusionMaterial::linearDiffusionMaterial(const std::string& name,
                                     const materialProperties& cl):
    speciesMaterial(name, cl),
    diffusivity(0.0), R(0.0), mu0(0.0), N(1)
{
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "refmu", mu0);
    double dN;
    muesli::assignValue(cl, "n", dN);
    N = (unsigned) dN;
}




linearDiffusionMaterial::~linearDiffusionMaterial()
{
}




bool linearDiffusionMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in linearDiffusionMaterial. Non-positive diffusivity";
    }

    return ret;
}




speciesMP* linearDiffusionMaterial::createMaterialPoint() const
{
    speciesMP *mp = new linearDiffusionMP(*this);
    return mp;
}




double linearDiffusionMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    if (p == PR_MUREF)
        ret = mu0;
    else
        std::cout << "\n Error in getProperty in linearDiffusionMaterial.";
    return ret;
}




void linearDiffusionMaterial::print(std::ostream &of) const
{
    of  << "\n   Material for linear diffusion of species on a host material"
        << "\n   Diffusivity       : " << diffusivity
        << "\n   Gas constant      : " << R
        << "\n   Ref. chemical pot.: " << mu0
        << "\n   Atoms/mol         : " << N;

    speciesMaterial::print(of);
}




void linearDiffusionMaterial::setRandom()
{
    diffusivity = muesli::randomUniform(1.0, 10.0);
    R           = muesli::randomUniform(1.0, 10.0);
    mu0         = muesli::randomUniform(250.0, 300.0);
    N           = (unsigned) muesli::randomUniform(250.0, 300.0);
}




bool linearDiffusionMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    speciesMP* p = this->createMaterialPoint();
    isok = p->testImplementation(os);
    delete p;
    return isok;
}




linearDiffusionMP::linearDiffusionMP(const linearDiffusionMaterial& mat):
    speciesMP(mat),
    theMaterial(&mat)
{
}




double linearDiffusionMP::chemicalCapacity() const
{
    //linear model, then the chemical capacity (d chi / d mu ) is constant
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();
    
    return N/(4.0*R*Theta0);
}




double linearDiffusionMP::chemicalCapacityDerivative() const
{
    return 0.0;
}




double linearDiffusionMP::chemicalPotential() const
{
    return mu_c;
}




double linearDiffusionMP::concentration() const
{
    //since the model is linear, chi (mu) = chi (mu0) + c0 * ( mu - mu0)
    //thus its derivative (the chemical capacity) is constant
    double mu0    = theMaterial->mu0;
    double R      = theMaterial->R;
    double N      = theMaterial->N;
    double Theta0 = theMaterial->referenceTemperature();
    
    double c0 = N /(4.0*R*Theta0);
    return 0.5*N + c0 * (mu_c-mu0);
}




void linearDiffusionMP::contractTangent(const ivector& na,
                                  const ivector& nb,
                                  double& tg) const
{
    tg = na.dot(nb)*mobility();
}




double linearDiffusionMP::freeEnergy() const
{
    double mu0    = theMaterial->mu0;
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();
    
    double c0 = N/(4.0*R*Theta0);
    double chi0 = 0.5*N;

    return mu0*chi_c + chi_c*(0.5*chi_c-chi0)/c0;
}




const linearDiffusionMaterial* linearDiffusionMP::getMaterial()
{
    return theMaterial;
}




double linearDiffusionMP::grandCanonicalPotential() const
{
    return freeEnergy() - mu_c*chi_c;
}




double linearDiffusionMP::kineticPotential() const
{
    return 0.5*mobility()*gradMu_c.squaredNorm();
}




void linearDiffusionMP::massFlux(ivector &q) const
{
    double m = mobility();
    q        = -m*gradMu_c;
}




void linearDiffusionMP::massFluxDerivative(ivector &qprime) const
{
    double mprime = mobilityDerivative();
    qprime        = -mprime*gradMu_c;
}




double linearDiffusionMP::mobility() const
{
    double D      = theMaterial->diffusivity;
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();
    
    return D*N/(2.0*R*Theta0);
}




double linearDiffusionMP::mobilityDerivative() const
{
    return 0.0;
}




void linearDiffusionMP::setRandom()
{
    speciesMP::setRandom();
}




nonLinearDiffusionMaterial::nonLinearDiffusionMaterial(const std::string& name,
                                     const materialProperties& cl):
    speciesMaterial(name, cl),
    diffusivity(0.0), R(0.0), mu0(0.0), N(1)
{
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "refmu", mu0);
    double dN;
    muesli::assignValue(cl, "n", dN);
    N = (unsigned) dN;
}




nonLinearDiffusionMaterial::~nonLinearDiffusionMaterial()
{
}




bool nonLinearDiffusionMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in nonLinearDiffusionMaterial. Non-positive diffusivity";
    }

    return ret;
}




speciesMP* nonLinearDiffusionMaterial::createMaterialPoint() const
{
    speciesMP *mp = new nonLinearDiffusionMP(*this);
    return mp;
}




double nonLinearDiffusionMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;
    
    if (p == PR_MUREF)
        ret = mu0;
    else
        std::cout << "\n Error in getProperty in nonLinearDiffusionMaterial.";
    return ret;
}




void nonLinearDiffusionMaterial::print(std::ostream &of) const
{
    of  << "\n   Material for non linear diffusion of species on a host material"
        << "\n   Diffusivity       : " << diffusivity
        << "\n   Gas constant      : " << R
        << "\n   Ref. chemical pot.: " << mu0
        << "\n   Atoms/mol         : " << N;

    speciesMaterial::print(of);
}




void nonLinearDiffusionMaterial::setRandom()
{
    diffusivity = muesli::randomUniform(1.0, 10.0);
    R           = muesli::randomUniform(1.0, 10.0);
    mu0         = muesli::randomUniform(250.0, 300.0);
    N           = (unsigned) muesli::randomUniform(250.0, 300.0);
}




bool nonLinearDiffusionMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    speciesMP* p = this->createMaterialPoint();
    isok = p->testImplementation(os);
    delete p;
    return isok;
}




nonLinearDiffusionMP::nonLinearDiffusionMP(const nonLinearDiffusionMaterial& mat):
    speciesMP(mat),
    theMaterial(&mat)
{
}




double nonLinearDiffusionMP::chemicalCapacity() const
{
    double mu0    = theMaterial->mu0;
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    double E  = exp((mu_c-mu0)/(R*Theta0));
    double Ep = E/(R*Theta0);
  
    return N*Ep/((1.0+E)*(1.0+E));
}




double nonLinearDiffusionMP::chemicalCapacityDerivative() const
{
    double mu0    = theMaterial->mu0;
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    double E   = exp((mu_c-mu0)/(R*Theta0));
    double Ep  = E/(R*Theta0);
    double Epp = E/((R*Theta0)*(R*Theta0));
    
    return N*Epp/((1.0+E)*(1.0+E)) - 2.0*N*Ep*Ep/((1.0+E)*(1.0+E)*(1.0+E));
}




double nonLinearDiffusionMP::chemicalPotential() const
{
    return mu_c;
}




double nonLinearDiffusionMP::concentration() const
{
    double mu0    = theMaterial->mu0;
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    double E = exp((mu_c-mu0)/(R*Theta0));
    
    return N * E/(1.0+E);
}




void nonLinearDiffusionMP::contractTangent(const ivector& na,
                                  const ivector& nb,
                                  double& tg) const
{
    tg = na.dot(nb)*mobility();
}




bool nonLinearDiffusionMP::freeEnergyCheck() const
{
    bool ret = true;
    
    double N      = theMaterial->N;
    double Theta  = chi_c/N;

    if (Theta == 1.0)
    {
        ret = false;
        std::cout << "Error in nonLinearDiffusionMP: Not a number in freeEnergy";
    }

    return ret;
}




double nonLinearDiffusionMP::freeEnergy() const
{
    double mu0    = theMaterial->mu0;
    double N      = theMaterial->N;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();
    
    double Theta  = chi_c/N;
    
    if (Theta == 1.0)
    {
        std::cout << "Error in nonLinearDiffusionMP: Not a number in freeEnergy.";

        return false;
    }
    
    else
    {
        return mu0*chi_c + R*Theta0*N * ( Theta*log(Theta) + (1.0 - Theta)*log(1.0 - Theta) );
    }
}




const nonLinearDiffusionMaterial* nonLinearDiffusionMP::getMaterial()
{
    return theMaterial;
}




double nonLinearDiffusionMP::grandCanonicalPotential() const
{
    return freeEnergy() - mu_c*chi_c;
}




double nonLinearDiffusionMP::kineticPotential() const
{
    return 0.5*mobility()*gradMu_c.squaredNorm();
}




void nonLinearDiffusionMP::massFlux(ivector &q) const
{
    double m = mobility();
    q        = -m*gradMu_c;
}




void nonLinearDiffusionMP::massFluxDerivative(ivector &qprime) const
{
    double mprime = mobilityDerivative();
    qprime        = -mprime*gradMu_c;
}




double nonLinearDiffusionMP::mobility() const
{
    double D      = theMaterial->diffusivity;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();
    
    return D*concentration()/(R*Theta0);
}




double nonLinearDiffusionMP::mobilityDerivative() const
{
    double D      = theMaterial->diffusivity;
    double R      = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    return D*chemicalCapacity()/(R*Theta0);
}




void nonLinearDiffusionMP::setRandom()
{
    speciesMP::setRandom();
}




dissolvedHydrogenMaterial::dissolvedHydrogenMaterial(const std::string& name):
    speciesMaterial(name),
    diffusivity(0.0), R(0.0),
    mu0L(0.0), mu0T(0.0),
    NL(1), NT(1)
{
}




dissolvedHydrogenMaterial::dissolvedHydrogenMaterial(const std::string& name,
                                                     const materialProperties& cl):
    speciesMaterial(name, cl),
    diffusivity(0.0), R(0.0),
    mu0L(0.0), mu0T(0.0),
    NL(1), NT(1)
{
    muesli::assignValue(cl, "diffusivity", diffusivity);
    muesli::assignValue(cl, "r", R);
    muesli::assignValue(cl, "mu0L", mu0L);
    muesli::assignValue(cl, "mu0T", mu0T);
    double dN;
    muesli::assignValue(cl, "nl", dN);  NL = (unsigned) dN;
    muesli::assignValue(cl, "nt", dN);  NT = (unsigned) dN;
}




dissolvedHydrogenMaterial::~dissolvedHydrogenMaterial()
{
}




bool dissolvedHydrogenMaterial::check() const
{
    bool ret = true;

    if (diffusivity <= 0.0)
    {
        ret = false;
        std::cout << "Error in dissolvedHydrogenMaterial. Non-positive diffusivity";
    }

    return ret;
}




speciesMP* dissolvedHydrogenMaterial::createMaterialPoint() const
{
    speciesMP *mp = new dissolvedHydrogenMP(*this);
    return mp;
}




double dissolvedHydrogenMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if (p == PR_MUREF)
        ret = mu0L;
    else
        std::cout << "\n Error in getProperty in dissolvedHydrogenMaterial.";
    return ret;
}




void dissolvedHydrogenMaterial::print(std::ostream &of) const
{
    of  << "\n   Material for hydrogen dissolved in a host metal"
        << "\n   Diffusivity              : " << diffusivity
        << "\n   Gas constant             : " << R
        << "\n   Ref. chemical pot. for L : " << mu0L
        << "\n   Ref. chemical pot. for T : " << mu0T
        << "\n   Atoms/mol for L          : " << NL
        << "\n   Atoms/mol for T          : " << NT;

    speciesMaterial::print(of);
}




void dissolvedHydrogenMaterial::setRandom()
{
    diffusivity = muesli::randomUniform(1.0, 10.0);
    R      = muesli::randomUniform(1.0, 10.0);
    mu0L   = muesli::randomUniform(250.0, 300.0);
    mu0T   = muesli::randomUniform(250.0, 300.0);
    NL     = (unsigned) muesli::randomUniform(250.0, 300.0);
    NT     = (unsigned) muesli::randomUniform(250.0, 300.0);
}




bool dissolvedHydrogenMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

    speciesMP* p = this->createMaterialPoint();
    isok = p->testImplementation(os);
    delete p;
    return isok;
}




dissolvedHydrogenMP::dissolvedHydrogenMP(const dissolvedHydrogenMaterial& mat):
    speciesMP(mat),
    theMaterial(&mat)
{
    mu_n  = mu_c  = theMaterial->mu0L;
    chi_n = chi_c = concentration();
}




double dissolvedHydrogenMP::chemicalPotential() const
{
    return mu_c;
}




// disolved hydrogen of L type
double dissolvedHydrogenMP::concentration() const
{
    double mu0L = theMaterial->mu0L;
    double R    = theMaterial->R;
    double NL   = (double) theMaterial->NL;
    double Theta0 = theMaterial->referenceTemperature();

    double EL = exp( (mu_c-mu0L)/(R*Theta0));
    return NL * EL/(1.0+EL);
}



double dissolvedHydrogenMP::concentrationT(double mu) const
{
    double mu0T = theMaterial->mu0T;
    double R    = theMaterial->R;
    double NT   = (double) theMaterial->NT;
    double Theta0 = theMaterial->referenceTemperature();

    double ET = exp( (mu-mu0T)/(R*Theta0));
    return NT * ET/(1.0+ET);
}




// effective chemical capacity
double dissolvedHydrogenMP::chemicalCapacity() const
{
    double R    = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    double mu0L = theMaterial->mu0L;
    double NL   = (double) theMaterial->NL;
    double EL = exp( (mu_c-mu0L)/(R*Theta0));
    double ELp = EL/(R*Theta0);
    double cLp = NL*ELp/((1.0 + EL)*(1.0+EL));

    double mu0T = theMaterial->mu0T;
    double NT   = (double) theMaterial->NT;
    double ET = exp( (mu_c-mu0T)/(R*Theta0));
    double ETp = ET/(R*Theta0);
    double cTp = NT*ETp/((1.0 + ET)*(1.0+ET));

    return cLp + cTp;
    
}



// effective chemical capacity derived wrt mu
double dissolvedHydrogenMP::chemicalCapacityDerivative() const
{
    return 0.0;
}




void dissolvedHydrogenMP::contractTangent(const ivector& na,
                                  const ivector& nb,
                                  double& tg) const
{
    tg = na.dot(nb)*mobility();
}




double dissolvedHydrogenMP::freeEnergy() const
{
    double R    = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    double mu0L = theMaterial->mu0L;
    double NL   = (double) theMaterial->NL;
    double EL   = exp( (mu_c-mu0L)/(R*Theta0));
    double ELp  = EL/(R*Theta0);
    double phiL = NL*ELp/((1.0 + EL)*(1.0+EL));

    double mu0T = theMaterial->mu0T;
    double NT   = (double) theMaterial->NT;
    double ET   = exp( (mu_c-mu0T)/(R*Theta0));
    double ETp  = ET/(R*Theta0);
    double phiT = NT*ETp/((1.0 + ET)*(1.0+ET));

    return phiL + phiT;
}




const dissolvedHydrogenMaterial* dissolvedHydrogenMP::getMaterial()
{
    return theMaterial;
}




void dissolvedHydrogenMP::getStateVariable(stateVariableName name, stateVariable& v) const
{
    if (name == SV_TRAPPED_H)
        v.d = concentrationT(mu_c);
    else if (name == SV_LATTICE_H)
        v.d = chi_c;
}




double dissolvedHydrogenMP::grandCanonicalPotential() const
{
    double chiT = concentrationT(mu_c);

    return freeEnergy() - mu_c*chi_c - mu_c*chiT ;
}




double dissolvedHydrogenMP::kineticPotential() const
{
    return 0.5*mobility()*gradMu_c.squaredNorm();
}




void dissolvedHydrogenMP::massFlux(ivector &q) const
{
    double m = mobility();
    q = -m*gradMu_c;
}




void dissolvedHydrogenMP::massFluxDerivative(ivector &qprime) const
{
    double mprime = mobilityDerivative();
    qprime = -mprime*gradMu_c;
}




double dissolvedHydrogenMP::mobility() const
{
    double D = theMaterial->diffusivity;
    double R = theMaterial->R;
    double Theta0 = theMaterial->referenceTemperature();

    return D/(2.0*R*Theta0);
}




double dissolvedHydrogenMP::mobilityDerivative() const
{
    return 0.0;
}




void dissolvedHydrogenMP::setRandom()
{
    speciesMP::setRandom();
}
